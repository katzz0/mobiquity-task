import React from 'react';
import './Loader.css';

class Loader extends React.PureComponent {
    render() {
        return (
            <div className={'Loader' + (this.props.className ? ' ' + this.props.className : '')}>
                <div className="rect rect1" />
                <div className="rect rect2" />
                <div className="rect rect3" />
                <div className="rect rect4" />
                <div className="rect rect5" />
            </div>
        );
    }
}

export default Loader;
