import React from 'react';
import PropTypes from 'prop-types';
import Loader from './Loader';
import './ListView.css';

class ListView extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            selectedRowIndex: null,
        };
    }

    handleRowClick(row, index) {
        this.setState({selectedRowIndex: index});
        if (this.props.onSelectedRowChange) {
            this.props.onSelectedRowChange(row.key);
        }
    }

    render() {
        return (
            <div className="ListView-wrapper">
                <ul className="ListView">
                    {!this.props.isLoading && this.props.rows.map((row, index) => {
                        const className = [
                            'ListView-item',
                            index === this.state.selectedRowIndex ? 'selected' : '',
                            this.props.highlightedRows && this.props.highlightedRows.includes(index) ? 'highlighted' : '',
                        ];

                        return <li
                            key={row.key}
                            className={className.join(' ').trim()}
                            onClick={() => this.handleRowClick(row, index)}
                        >
                            {this.props.columns.map((columnName, index) => <dl
                                key={index}
                                className="entry"
                            >
                                <dt className="name">{columnName}</dt>
                                <dd className="value">{row.data[index]}</dd>
                            </dl>)}
                        </li>
                    })}
                </ul>
                {this.props.isLoading && <Loader className="loader" />}
            </div>
        );
    }
}

ListView.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.string).isRequired,
    rows: PropTypes.arrayOf(
        PropTypes.shape({
            key: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
            data: PropTypes.arrayOf(
                PropTypes.oneOfType([
                    PropTypes.string,
                    PropTypes.node,
                ])
            ).isRequired
        })
    ).isRequired,
    isLoading: PropTypes.bool,
    highlightedRows: PropTypes.arrayOf(PropTypes.number),
    onSelectedRowChange: PropTypes.func,
};

export default ListView;
