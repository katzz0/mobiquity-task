import React from 'react';
import ListView from './ListView';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';

let columns = ['Column1', 'Column2', 'Column3'];
let rows = [
    {key: 'key1', data: ['data11', 'data12', 'data13']},
    {key: 'key2', data: ['data21', 'data22', 'data23']},
    {key: 'key3', data: ['data31', 'data32', 3]},
];

it('should render content', () => {
    const component = renderer.create(
        <ListView
            columns={columns}
            rows={rows}
            highlightedRows={[0, 2]}
        />,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('should show data', () => {
    const mockOnSelectedRowChange = jest.fn();

    const component = shallow(
        <ListView
            columns={columns}
            rows={rows}
            onSelectedRowChange={mockOnSelectedRowChange}
        />
    );

    const row1 = component.findWhere(n => n.key() === 'key1');
    const row2 = component.findWhere(n => n.key() === 'key2');
    const row3 = component.findWhere(n => n.key() === 'key3');

    expect(row1.text()).toBe(['Column1', 'data11', 'Column2', 'data12', 'Column3', 'data13'].join(''));
    expect(row2.text()).toBe(['Column1', 'data21', 'Column2', 'data22', 'Column3', 'data23'].join(''));
    expect(row3.text()).toBe(['Column1', 'data31', 'Column2', 'data32', 'Column3', 3].join(''));
});

it('should select a row when clicked', () => {
    const mockOnSelectedRowChange = jest.fn();

    const component = shallow(
        <ListView
            columns={columns}
            rows={rows}
            onSelectedRowChange={mockOnSelectedRowChange}
        />
    );

    const row = component.findWhere(n => n.key() === 'key2');
    row.simulate('click');

    expect(mockOnSelectedRowChange).toHaveBeenCalledWith('key2');
    expect(component.instance().state.selectedRowIndex).toBe(1);
    expect(component.findWhere(n => n.key() === 'key2').hasClass('selected')).toBe(true);
});

it('should highlight rows', () => {
    const mockOnSelectedRowChange = jest.fn();

    const component = shallow(
        <ListView
            columns={columns}
            rows={rows}
            highlightedRows={[1, 2]}
            onSelectedRowChange={mockOnSelectedRowChange}
        />
    );

    const row2 = component.findWhere(n => n.key() === 'key2');
    const row3 = component.findWhere(n => n.key() === 'key3');

    expect(row2.hasClass('highlighted')).toBe(true);
    expect(row3.hasClass('highlighted')).toBe(true);
});

it('should show loader instead of data', () => {
    const mockOnSelectedRowChange = jest.fn();

    const component = shallow(
        <ListView
            columns={columns}
            rows={rows}
            highlightedRows={[1, 2]}
            onSelectedRowChange={mockOnSelectedRowChange}
            isLoading={true}
        />
    );

    expect(component.find('.loader')).toBeDefined();

    const row1 = component.findWhere(n => n.key() === 'key1');
    const row2 = component.findWhere(n => n.key() === 'key2');
    const row3 = component.findWhere(n => n.key() === 'key3');

    expect(row1.length).toBe(0);
    expect(row2.length).toBe(0);
    expect(row3.length).toBe(0);
});
