import React from 'react';
import PropTypes from 'prop-types';
import Loader from './Loader';
import './DataGrid.css';

class DataGrid extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            selectedRowIndex: null,
        };
    }

    handleRowClick(row, index) {
        this.setState({selectedRowIndex: index});
        if (this.props.onSelectedRowChange) {
            this.props.onSelectedRowChange(row.key);
        }
    }

    render() {
        return (
            <div className="DataGrid-wrapper">
                <table className="DataGrid">
                    <thead>
                    <tr>
                        {['#'].concat(this.props.columns).map(columnName => <th key={columnName}>{columnName}</th>)}
                    </tr>
                    </thead>
                    <tbody>
                    {!this.props.isLoading && this.props.rows.map((row, index) => {
                        const className = [
                            index === this.state.selectedRowIndex ? 'selected' : '',
                            this.props.highlightedRows && this.props.highlightedRows.includes(index) ? 'highlighted' : '',
                        ];

                        return <tr
                            key={row.key}
                            className={className.join(' ').trim()}
                            onClick={() => this.handleRowClick(row, index)}
                        >
                            <th>{index + 1}</th>
                            {row.data.map(cell => <td key={cell}>{cell}</td>)}
                        </tr>
                    })}
                    </tbody>
                </table>
                {this.props.isLoading && <Loader className="loader" />}
            </div>
        );
    }
}

DataGrid.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.string).isRequired,
    rows: PropTypes.arrayOf(
        PropTypes.shape({
            key: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
            data: PropTypes.arrayOf(
                PropTypes.oneOfType([
                    PropTypes.string,
                    PropTypes.node,
                ])
            ).isRequired
        })
    ).isRequired,
    isLoading: PropTypes.bool,
    highlightedRows: PropTypes.arrayOf(PropTypes.number),
    onSelectedRowChange: PropTypes.func,
};

export default DataGrid;
