import React from 'react';
import Toaster from './Toaster';
import renderer from 'react-test-renderer';

test('SidePanel should render content', () => {
    let component = renderer.create(<Toaster text="Some text" visible={true} />);

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
