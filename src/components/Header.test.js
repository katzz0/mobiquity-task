import React from 'react';
import Header from './Header';
import renderer from 'react-test-renderer';

test('Header should render content with a title', () => {
    const component = renderer.create(<Header title="Application" />);

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
