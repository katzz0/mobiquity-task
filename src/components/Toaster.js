import React from 'react';
import PropTypes from 'prop-types';
import {CSSTransition} from 'react-transition-group';
import './Toaster.css';

export class Toaster extends React.PureComponent {
    render() {
        return (
            <CSSTransition
                in={this.props.visible}
                timeout={1000}
                classNames="Toaster"
                unmountOnExit>
                <div className="Toaster">
                    {this.props.text}
                </div>
            </CSSTransition>
        );
    }
}

Toaster.propTypes = {
    text: PropTypes.string,
    visible: PropTypes.bool.isRequired,
};

export default Toaster;
