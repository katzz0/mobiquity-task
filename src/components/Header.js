import React from 'react';
import PropTypes from 'prop-types';
import './Header.css';

class Header extends React.PureComponent {
    render() {
        return (
            <header className="Header">
                <div className="Header-content-wrapper">
                    <h1 className="Header-title">
                        <span className="Header-title-decorator">{this.props.title}</span>
                    </h1>
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
};

export default Header;
