import {LAYOUT_LARGE} from '../services/layout';

export default {
    champions: {
        isFetching: false,
        data: [],
        errorMessage: null,
    },
    seasonWinners: {
        isFetching: false,
        data: [],
        season: null,
        errorMessage: null,
    },
    layout: LAYOUT_LARGE,
};
