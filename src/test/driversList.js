export default [
    {
        driverId: 'alonso',
        permanentNumber: '14',
        code: 'ALO',
        url: 'http://en.wikipedia.org/wiki/Fernando_Alonso',
        givenName: 'Fernando',
        familyName: 'Alonso',
        dateOfBirth: '1981-07-29',
        nationality: 'Spanish'
    },
    {
        driverId: 'alonso',
        permanentNumber: '14',
        code: 'ALO',
        url: 'http://en.wikipedia.org/wiki/Fernando_Alonso',
        givenName: 'Fernando',
        familyName: 'Alonso',
        dateOfBirth: '1981-07-29',
        nationality: 'Spanish'
    },
    {
        driverId: 'raikkonen',
        permanentNumber: '7',
        code: 'RAI',
        url: 'http://en.wikipedia.org/wiki/Kimi_R%C3%A4ikk%C3%B6nen',
        givenName: 'Kimi',
        familyName: 'Räikkönen',
        dateOfBirth: '1979-10-17',
        nationality: 'Finnish'
    },
    {
        driverId: 'hamilton',
        permanentNumber: '44',
        code: 'HAM',
        url: 'http://en.wikipedia.org/wiki/Lewis_Hamilton',
        givenName: 'Lewis',
        familyName: 'Hamilton',
        dateOfBirth: '1985-01-07',
        nationality: 'British'
    },
    {
        driverId: 'button',
        permanentNumber: '22',
        code: 'BUT',
        url: 'http://en.wikipedia.org/wiki/Jenson_Button',
        givenName: 'Jenson',
        familyName: 'Button',
        dateOfBirth: '1980-01-19',
        nationality: 'British'
    },
    {
        driverId: 'vettel',
        permanentNumber: '5',
        code: 'VET',
        url: 'http://en.wikipedia.org/wiki/Sebastian_Vettel',
        givenName: 'Sebastian',
        familyName: 'Vettel',
        dateOfBirth: '1987-07-03',
        nationality: 'German'
    }
];
