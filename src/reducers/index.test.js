import {champions, seasonWinners, layout} from './index';
import * as ActionTypes from '../actions';
import driversList from '../test/driversList';
import {LAYOUT_LARGE} from '../services/layout';

const mockChampionsData = {
    MRData: {
        StandingsTable: {
            StandingsLists: driversList.map((driver, index) => ({
                season: 2005 + index,
                DriverStandings: {
                    0: {
                        Driver: driver,
                    }
                }
            }))
        }
    }
};
const championsEntries = driversList.map((driver, index) => ({driver, season: 2005 + index}));
const mockWinnersData = {
    MRData: {
        RaceTable: {
            Races: driversList.map((driver, index) => ({
                Results: {
                    0: {
                        Driver: driver,
                    }
                }
            }))
        }
    }
};

test('champions reducer should return a default state', () => {
    const state = champions(undefined, {});
    expect(state).toMatchObject({
        isFetching: false,
        data: [],
        errorMessage: null,
        startYear: null,
        endYear: null,
    });
});


test('champions reducer should set isFetching true', () => {
    const state = champions(undefined, {type: ActionTypes.CHAMPIONS_FETCH_REQUESTED});
    expect(state).toMatchObject({
        isFetching: true,
        data: [],
        errorMessage: null,
        startYear: null,
        endYear: null,
    });
});

test('champions reducer should set start and end years', () => {
    const state = champions(undefined, {
        type: ActionTypes.CHAMPIONS_FETCH_REQUESTED,
        startYear: 2005,
        endYear: 2010,
    });

    expect(state).toMatchObject({
        isFetching: true,
        data: [],
        errorMessage: null,
        startYear: 2005,
        endYear: 2010,
    });
});

test('champions reducer should set isFetching false and an error message when data retrieval process is failed', () => {
    const state = champions({
        isFetching: true,
        data: [],
        errorMessage: null,
        startYear: null,
        endYear: null,
    }, {
        type: ActionTypes.CHAMPIONS_FETCH_FAILED,
        message: 'error'
    });

    expect(state).toMatchObject({
        isFetching: false,
        data: [],
        errorMessage: 'error',
        startYear: null,
        endYear: null,
    });
});

test('champions reducer should set isFetching false and received data when data has been received', () => {
    const state = champions(undefined, {type: ActionTypes.CHAMPIONS_FETCH_SUCCEEDED, data: mockChampionsData});
    expect(state).toMatchObject({
        isFetching: false,
        data: championsEntries,
        errorMessage: null,
        startYear: null,
        endYear: null,
    });
});

test('champions reducer should set isFetching false and filter received data when data has been received and start and end dates are set', () => {
    const state = champions({
        startYear: 2010,
        endYear: 2020,
    }, {type: ActionTypes.CHAMPIONS_FETCH_SUCCEEDED, data: mockChampionsData});

    expect(state.data.length).toBe(driversList.length - 5);
});

test('champions reducer should clear an error message', () => {
    const state = champions({
        isFetching: false,
        data: [],
        errorMessage: 'some error message',
        startYear: null,
        endYear: null,
    }, {type: ActionTypes.CHAMPIONS_CLEAR_ERROR_MESSAGE});

    expect(state.errorMessage).toBe(null);
});

test('seasonWinners reducer should return a default state', () => {
    const state = seasonWinners(undefined, {});
    expect(state).toMatchObject({
        isFetching: false,
        data: [],
        season: null,
        errorMessage: null,
        offset: null,
        limit: null,
    });
});


test('seasonWinners reducer should set isFetching, season, offset and limit', () => {
    let state = seasonWinners(undefined, {type: ActionTypes.SEASON_WINNERS_FETCH_REQUESTED});
    expect(state).toMatchObject({
        isFetching: true,
        data: [],
        season: null,
        errorMessage: null,
        offset: null,
        limit: null,
    });

    state = seasonWinners(undefined, {type: ActionTypes.SEASON_WINNERS_FETCH_REQUESTED, year: 2006});
    expect(state.season).toBe(2006);

    state = seasonWinners(undefined, {type: ActionTypes.SEASON_WINNERS_FETCH_REQUESTED, offset: 10});
    expect(state.offset).toBe(10);

    state = seasonWinners(undefined, {type: ActionTypes.SEASON_WINNERS_FETCH_REQUESTED, limit: 20});
    expect(state.limit).toBe(20);

    state = seasonWinners(undefined, {
        type: ActionTypes.SEASON_WINNERS_FETCH_REQUESTED,
        year: 2010,
        offset: 10,
        limit: 20,
    });
    expect(state).toMatchObject({
        isFetching: true,
        data: [],
        season: 2010,
        errorMessage: null,
        offset: 10,
        limit: 20,
    });
});

test('seasonWinners reducer should set isFetching to false and an error message when data retrieval process is failed', () => {
    const state = seasonWinners({
        isFetching: true,
        data: [],
        season: 2006,
        errorMessage: null,
        offset: 100,
        limit: 500,
    }, {
        type: ActionTypes.SEASON_WINNERS_FETCH_FAILED,
        message: 'error'
    });

    expect(state).toMatchObject({
        isFetching: false,
        data: [],
        season: null,
        errorMessage: 'error',
        offset: null,
        limit: null,
    });
});

test('seasonWinners reducer should set isFetching to false and received data when data has been received', () => {
    const state = seasonWinners(undefined, {type: ActionTypes.SEASON_WINNERS_FETCH_SUCCEEDED, data: mockWinnersData});
    expect(state).toMatchObject({
        isFetching: false,
        data: driversList,
        season: null,
        errorMessage: null,
        offset: null,
        limit: null,
    });
});

test('seasonWinners reducer should clear an error message', () => {
    const state = seasonWinners({
        isFetching: false,
        data: [],
        season: null,
        errorMessage: 'some error message',
        offset: null,
        limit: null,
    }, {type: ActionTypes.SEASON_WINNERS_CLEAR_ERROR_MESSAGE});

    expect(state.errorMessage).toBe(null);
});

test('layout reducer should return a default state', () => {
    const state = layout(undefined, {});
    expect(state).toBe(LAYOUT_LARGE);
});

test('layout reducer should set the provided layout', () => {
    const state = layout(undefined, {type: ActionTypes.LAYOUT_CHANGE, layout: 'large'});
    expect(state).toBe('large');
});
