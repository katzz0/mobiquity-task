import * as ActionTypes from '../actions';
import {combineReducers} from 'redux';
import {LAYOUT_LARGE} from '../services';

export const champions = (state = {
    isFetching: false,
    data: [],
    errorMessage: null,
    startYear: null,
    endYear: null,
}, action) => {
    switch (action.type) {
        case ActionTypes.CHAMPIONS_FETCH_REQUESTED:
            return {
                ...state,
                isFetching: true,
                startYear: action.startYear || null,
                endYear: action.endYear || null,
            };

        case ActionTypes.CHAMPIONS_FETCH_SUCCEEDED:
            let data = [];
            if (action.data['MRData'] && action.data['MRData']['StandingsTable'] &&
                action.data['MRData']['StandingsTable']['StandingsLists'] &&
                Array.isArray(action.data['MRData']['StandingsTable']['StandingsLists'])
            ) {
                data = action.data['MRData']['StandingsTable']['StandingsLists']
                    .filter(standing => {
                        if (state.startYear && standing.season < state.startYear) {
                            return false;
                        }

                        if (state.endYear && standing.season > state.endYear) {
                            return false;
                        }

                        return true;
                    })
                    .map(standing => ({driver: standing['DriverStandings'][0]['Driver'], season: standing.season}));
            }

            return {
                ...state,
                isFetching: false,
                data,
            };

        case ActionTypes.CHAMPIONS_FETCH_FAILED:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.message,
                startYear: null,
                endYear: null,
            };

        case ActionTypes.CHAMPIONS_CLEAR_ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: null,
            };

        default:
            return state;
    }
};

export const seasonWinners = (state = {
    isFetching: false,
    data: [],
    season: null,
    errorMessage: null,
    offset: null,
    limit: null,
}, action) => {
    switch (action.type) {
        case ActionTypes.SEASON_WINNERS_FETCH_REQUESTED:
            return {
                ...state,
                isFetching: true,
                season: action.year || null,
                offset: action.offset || null,
                limit: action.limit || null,
            };

        case ActionTypes.SEASON_WINNERS_FETCH_SUCCEEDED:
            let data = [];
            if (action.data['MRData'] && action.data['MRData']['RaceTable'] &&
                action.data['MRData']['RaceTable']['Races'] &&
                Array.isArray(action.data['MRData']['RaceTable']['Races'])
            ) {
                data = action.data['MRData']['RaceTable']['Races'].map(race => race['Results'][0]['Driver']);
            }

            return {
                ...state,
                isFetching: false,
                data,
            };

        case ActionTypes.SEASON_WINNERS_FETCH_FAILED:
            return {
                ...state,
                isFetching: false,
                season: null,
                errorMessage: action.message,
                offset: null,
                limit: null,
            };

        case ActionTypes.SEASON_WINNERS_CLEAR_ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: null,
            };

        default:
            return state;
    }
};

export const layout = (state = LAYOUT_LARGE, action) => {
    if (action.type === ActionTypes.LAYOUT_CHANGE) {
        return action.layout;
    }

    return state;
};

const rootReducer = combineReducers({
    champions,
    seasonWinners,
    layout,
});

export default rootReducer;
