import {put, call, takeLatest} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import * as Api from '../services';
import * as ActionTypes from '../actions';

export function* fetchChampions() {
    try {
        const champions = yield call(Api.fetchChampions);
        yield put({type: ActionTypes.CHAMPIONS_FETCH_SUCCEEDED, data: champions});
    } catch (e) {
        yield put({type: ActionTypes.CHAMPIONS_FETCH_FAILED, message: 'Failed to fetch champions list'});
        yield call(delay, 2000);
        yield put({type: ActionTypes.CHAMPIONS_CLEAR_ERROR_MESSAGE});
    }
}

export function* fetchSeasonWinners(action) {
    try {
        const winners = yield call(Api.fetchSeasonWinners, action.year, action.offset, action.limit);
        yield put({type: ActionTypes.SEASON_WINNERS_FETCH_SUCCEEDED, data: winners});
    } catch (e) {
        yield put({type: ActionTypes.SEASON_WINNERS_FETCH_FAILED, message: 'Failed to fetch season winners list'});
        yield call(delay, 2000);
        yield put({type: ActionTypes.SEASON_WINNERS_CLEAR_ERROR_MESSAGE});
    }
}

export default function* rootSaga() {
    yield takeLatest(ActionTypes.CHAMPIONS_FETCH_REQUESTED, fetchChampions);
    yield takeLatest(ActionTypes.SEASON_WINNERS_FETCH_REQUESTED, fetchSeasonWinners);
}
