import {call, put, takeLatest} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import rootSaga, {fetchChampions, fetchSeasonWinners} from './index';
import * as ActionTypes from '../actions';
import * as Api from '../services';

it('should fetch champions data', () => {
    const gen = fetchChampions();
    expect(gen.next().value).toEqual(call(Api.fetchChampions));
});

it('should yield CHAMPIONS_FETCH_FAILED action and an error message when an exception has been thrown', () => {
    const gen = fetchChampions();

    expect(gen.next().value).toEqual(call(Api.fetchChampions));
    expect(gen.throw(new Error('some error text')).value).toEqual(put({
        type: ActionTypes.CHAMPIONS_FETCH_FAILED,
        message: 'Failed to fetch champions list',
    }));
    expect(gen.next().value).toEqual(call(delay, 2000));
    expect(gen.next().value).toEqual(put({type: ActionTypes.CHAMPIONS_CLEAR_ERROR_MESSAGE}));
});

it('should yield CHAMPIONS_FETCH_SUCCEEDED action when data has been successfully received', () => {
    const gen = fetchChampions();

    expect(gen.next().value).toEqual(call(Api.fetchChampions));
    expect(gen.next(['some data']).value).toEqual(put({
        type: ActionTypes.CHAMPIONS_FETCH_SUCCEEDED,
        data: ['some data'],
    }));
});

it('should fetch season winners data', () => {
    const gen = fetchSeasonWinners({year: 2010, offset: 200, limit: 300});
    expect(gen.next().value).toEqual(call(Api.fetchSeasonWinners, 2010, 200, 300));
});

it('should yield SEASON_WINNERS_FETCH_FAILED action and an error message when an exception has been thrown', () => {
    const gen = fetchSeasonWinners({});

    expect(gen.next().value).toEqual(call(Api.fetchSeasonWinners, undefined, undefined, undefined));
    expect(gen.throw(new Error('some error text')).value).toEqual(put({
        type: ActionTypes.SEASON_WINNERS_FETCH_FAILED,
        message: 'Failed to fetch season winners list',
    }));
    expect(gen.next().value).toEqual(call(delay, 2000));
    expect(gen.next().value).toEqual(put({type: ActionTypes.SEASON_WINNERS_CLEAR_ERROR_MESSAGE}));
});

it('should yield SEASON_WINNERS_FETCH_SUCCEEDED action when data has been successfully received', () => {
    const gen = fetchSeasonWinners({});

    expect(gen.next().value).toEqual(call(Api.fetchSeasonWinners, undefined, undefined, undefined));
    expect(gen.next(['some data']).value).toEqual(put({
        type: ActionTypes.SEASON_WINNERS_FETCH_SUCCEEDED,
        data: ['some data'],
    }));
});

it('should take the latest of fetchChampions and fetchSeasonWinners', () => {
    const gen = rootSaga();
    expect(gen.next().value).toEqual(takeLatest(ActionTypes.CHAMPIONS_FETCH_REQUESTED, fetchChampions));
    expect(gen.next().value).toEqual(takeLatest(ActionTypes.SEASON_WINNERS_FETCH_REQUESTED, fetchSeasonWinners));
});
