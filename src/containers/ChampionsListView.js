import React from 'react';
import {connect} from 'react-redux';
import ListView from '../components/ListView';
import {requestSeasonWinners} from '../actions';

export const mapStateToProps = state => {
    return {
        columns: ['', '', '', 'season:'],
        rows: state.champions.data.map(entry => ({
            key: entry.season,
            data: [
                <a href={entry.driver.url} target="_blank">
                    {entry.driver.givenName + ' ' + entry.driver.familyName}
                </a>,
                entry.driver.dateOfBirth,
                entry.driver.nationality,
                entry.season,
            ],
        })),
        isLoading: state.champions.isFetching,
    };
};

export const mapDispatchToProps = dispatch => {
    return {
        onSelectedRowChange: season => {
            dispatch(requestSeasonWinners(season))
        }
    }
};

const ChampionsListView = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListView);

export default ChampionsListView;
