import React from 'react';
import {Provider} from 'react-redux';
import MainContent from './MainContent';
import configureStore from 'redux-mock-store';
import renderer from 'react-test-renderer';
import initialState from '../test/mockedState';
import {LAYOUT_SMALL} from '../services/layout';

const mockStore = configureStore();

test('MainContent should render content for the large screen', () => {
    const store = mockStore(initialState);
    const wrapper = renderer.create(
        <Provider store={store}>
            <MainContent />
        </Provider>
    );

    let tree = wrapper.toJSON();
    expect(tree).toMatchSnapshot();
});

test('MainContent should render content for the small screen', () => {
    const state = Object.assign({}, initialState, {layout: LAYOUT_SMALL});
    const store = mockStore(state);
    const wrapper = renderer.create(
        <Provider store={store}>
            <MainContent />
        </Provider>
    );

    let tree = wrapper.toJSON();
    expect(tree).toMatchSnapshot();
});
