import React from 'react';
import {connect} from 'react-redux';
import DataGrid from '../components/DataGrid';
import {getSeasonChampionIndexes} from '../selectors';

export const mapStateToProps = state => {
    return {
        columns: ['Full Name', 'Nationality', 'Date of birth'],
        rows: state.seasonWinners.data.map((driver, index) => ({
            key: index,
            data: [
                <a href={driver.url} target="_blank">
                    {driver.givenName + ' ' + driver.familyName}
                </a>,
                driver.nationality,
                driver.dateOfBirth,
            ],
        })),
        highlightedRows: getSeasonChampionIndexes(state),
        isLoading: state.seasonWinners.isFetching,
    };
};

const SeasonWinnersDataGrid = connect(
    mapStateToProps,
)(DataGrid);

export default SeasonWinnersDataGrid;
