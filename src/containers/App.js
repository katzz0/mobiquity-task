import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../components/Header';
import MainContent from './MainContent';
import ErrorToaster from './ErrorToaster';
import {requestChampions, layoutChange} from '../actions';
import {getLayout} from '../services';
import './App.css';

class App extends React.PureComponent {
    constructor(props) {
        super(props);

        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleWindowResize();
    }

    componentDidMount() {
        this.props.dispatch(requestChampions(2005, 2015));
        window.addEventListener('resize', this.handleWindowResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize);
    }

    handleWindowResize() {
        const layout = getLayout();
        if (this.props.currentLayout !== layout) {
            this.props.dispatch(layoutChange(layout));
        }
    }

    render() {
        return (
            <div className="App">
                <Header title="F1 world champions" logo="" />
                <div className="App-body-wrapper">
                    <MainContent className="App-body" />
                </div>
                <ErrorToaster />
            </div>
        );
    }
}

App.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    currentLayout: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
    return {
        isFetching: state.champions.isFetching,
        currentLayout: state.layout,
    };
};

export default connect(mapStateToProps)(App);
