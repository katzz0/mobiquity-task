import React from 'react';
import {connect} from 'react-redux';
import ListView from '../components/ListView';
import {getSeasonChampionIndexes} from '../selectors';

export const mapStateToProps = state => {
    return {
        columns: ['', '', ''],
        rows: state.seasonWinners.data.map((driver, index) => ({
            key: index,
            data: [
                <a href={driver.url} target="_blank">
                    {driver.givenName + ' ' + driver.familyName}
                </a>,
                driver.nationality,
                driver.dateOfBirth,
            ],
        })),
        highlightedRows: getSeasonChampionIndexes(state),
        isLoading: state.seasonWinners.isFetching,
    };
};

const SeasonWinnersListView = connect(
    mapStateToProps,
)(ListView);

export default SeasonWinnersListView;
