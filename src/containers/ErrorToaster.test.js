import {mapStateToProps} from './ErrorToaster';

test('ErrorToaster should get right props', () => {
    const result = mapStateToProps({
        champions: {
            errorMessage: 'Some error message',
        },
        seasonWinners: {
            errorMessage: null,
        }
    });

    expect(result).toMatchObject({
        text: 'Some error message',
        visible: true,
    });
});
