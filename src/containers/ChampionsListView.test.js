import {mapStateToProps, mapDispatchToProps} from './ChampionsListView';
import {requestSeasonWinners} from '../actions';
import driversList from '../test/driversList';

const drivers = driversList.map((driver, index) => ({driver, season: 2005 + index}));

test('mapStateToProps should return columns, rows and isLoading state', () => {
    const props = mapStateToProps({champions: {isFetching: false, data: drivers}});

    expect(Array.isArray(props.columns)).toBe(true);
    expect(props.columns.length).toBe(4);

    expect(Array.isArray(props.rows)).toBe(true);
    expect(props.rows.length).toBe(drivers.length);
    expect(props.rows[0].key).toBe(2005);
    expect(props.rows[0].data.length).toBe(4);

    expect(props.isLoading).toBe(false);
});

test('mapStateToProps should isLoading = true', () => {
    const props = mapStateToProps({champions: {isFetching: true, data: drivers}});
    expect(props.isLoading).toBe(true);
});

test('ChampionsDataGrid should dispatch an action when a row has been selected', () => {
    const mockDispatch = jest.fn();

    const props = mapDispatchToProps(mockDispatch);
    expect(typeof props.onSelectedRowChange).toBe('function');

    props.onSelectedRowChange(2010);
    expect(mockDispatch).toHaveBeenCalledWith(requestSeasonWinners(2010));
});
