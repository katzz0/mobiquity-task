import {mapStateToProps} from './SeasonWinnersDataGrid';
import driversList from '../test/driversList';

const championDrivers = driversList.map((driver, index) => ({driver, season: 2005 + index}));

test('mapStateToProps should return columns, rows and highlightedRows', () => {
    const props = mapStateToProps({
        champions: {data: championDrivers},
        seasonWinners: {
            isFetching: false,
            data: driversList,
            season: 2010,
        }
    });

    expect(Array.isArray(props.columns)).toBe(true);
    expect(props.columns.length).toBe(3);

    expect(Array.isArray(props.rows)).toBe(true);
    expect(props.rows.length).toBe(driversList.length);
    expect(props.rows[0].key).toBe(0);
    expect(props.rows[0].data.length).toBe(3);

    expect(Array.isArray(props.highlightedRows)).toBe(true);
    expect(props.highlightedRows.length).toBe(1);
    expect(props.highlightedRows[0]).toBe(5);

    expect(props.isLoading).toBe(false);
});

test('mapStateToProps should isLoading = true', () => {
    const props = mapStateToProps({
        champions: {data: championDrivers},
        seasonWinners: {
            isFetching: true,
            data: driversList,
            season: 2010,
        }
    });
    expect(props.isLoading).toBe(true);
});
