import React from 'react';
import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import configureStore from 'redux-mock-store';
import App from './App';
import initialState from '../test/mockedState';

const mockStore = configureStore();

it('renders without crashing', () => {
    const store = mockStore(initialState);

    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
          <App />
      </Provider>,
    div);
    ReactDOM.unmountComponentAtNode(div);
});
