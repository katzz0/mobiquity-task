import React from 'react';
import PropTypes from 'prop-types';
import ChampionsDataGrid from '../containers/ChampionsDataGrid';
import SeasonWinnersDataGrid from '../containers/SeasonWinnersDataGrid';
import {LAYOUT_LARGE} from '../services/layout';
import ChampionsListView from './ChampionsListView';
import SeasonWinnersListView from './SeasonWinnersListView';
import {connect} from 'react-redux';
import './MainContent.css';

class MainContent extends React.PureComponent {
    constructor(props) {
        super(props);

        this.winnersHeaderRef = React.createRef();
    }

    render() {
        return (
            <main className={'MainContent ' + this.props.className}>
                <div className='MainContent-wrapper'>
                    <div className='content'>
                        <div className='champions'>
                            <h2>
                                List of F1 champions
                                {this.props.startYear ? ' from ' + this.props.startYear : ''}
                                {this.props.endYear ? ' until ' + this.props.endYear : ''}
                            </h2>
                            {
                                this.props.currentLayout === LAYOUT_LARGE ?
                                    <ChampionsDataGrid /> :
                                    <ChampionsListView />
                            }
                        </div>
                        {
                            this.props.season ? (
                                <div className='season-winners'>
                                    <h2 ref={this.winnersHeaderRef}>Winners of {this.props.season} season</h2>
                                    {
                                        this.props.currentLayout === LAYOUT_LARGE ?
                                            <SeasonWinnersDataGrid /> :
                                            <SeasonWinnersListView />
                                    }
                                </div>
                            ) : ''
                        }
                    </div>
                </div>
            </main>
        );
    }

    componentDidUpdate() {
        if (this.props.season && this.winnersHeaderRef) {
            this.winnersHeaderRef.current.scrollIntoView(true);
        }
    }
}

MainContent.propTypes = {
    className: PropTypes.string,
    season: PropTypes.string,
    startYear: PropTypes.number,
    endYear: PropTypes.number,
    currentLayout: PropTypes.string.isRequired,
};

export default connect((state) => ({
    startYear: state.champions.startYear,
    endYear: state.champions.endYear,
    season: state.seasonWinners.season,
    currentLayout: state.layout,
    isSeasonWinnersDataReady: !state.seasonWinners.isFetching && state.seasonWinners.season,
}))(MainContent);
