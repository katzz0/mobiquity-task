import React from 'react';
import {connect} from 'react-redux';
import DataGrid from '../components/DataGrid';
import {requestSeasonWinners} from '../actions';

export const mapStateToProps = state => {
    return {
        columns: ['Season', 'Full Name', 'Nationality', 'Date of birth'],
        rows: state.champions.data.map(entry => ({
            key: entry.season,
            data: [
                entry.season,
                <a href={entry.driver.url} target="_blank">
                    {entry.driver.givenName + ' ' + entry.driver.familyName}
                </a>,
                entry.driver.nationality,
                entry.driver.dateOfBirth,
            ],
        })),
        isLoading: state.champions.isFetching,
    };
};

export const mapDispatchToProps = dispatch => {
    return {
        onSelectedRowChange: season => {
            dispatch(requestSeasonWinners(season))
        }
    }
};

const ChampionsDataGrid = connect(
    mapStateToProps,
    mapDispatchToProps,
)(DataGrid);

export default ChampionsDataGrid;
