import {connect} from 'react-redux';
import Toaster from '../components/Toaster';

export const mapStateToProps = state => {
    return {
        text: state.champions.errorMessage || state.seasonWinners.errorMessage,
        visible: !!(state.champions.errorMessage || state.seasonWinners.errorMessage),
    };
};

const ErrorToaster = connect(
    mapStateToProps,
)(Toaster);

export default ErrorToaster;
