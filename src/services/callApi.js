import "isomorphic-fetch";

const API_ROOT = 'http://ergast.com/api/';

export const makeQueryString = (offset, limit = 1000) => {
    const queryString = [
        offset ? 'offset=' + offset : '',
        limit ? 'limit=' + limit : '',
    ].filter(p => p.length).join('&');

    return queryString ? '?' + queryString : '';
};

const callApi = (endpoint, offset, limit) => {
    const fullUrl = API_ROOT + 'f1/' + endpoint + '.json' + makeQueryString(offset, limit);

    try {
        return fetch(fullUrl).then(response => {
            if (!response.ok) {
                return Promise.reject();
            }

            return response.json();
        }).catch(error => Promise.reject({error: (error && error.message) || "Something bad happened"}));
    } catch (e) {
        return Promise.resolve({error: "Something bad happened"});
    }

};

// api services
export const fetchChampions = () => callApi('driverstandings/1');
export const fetchSeasonWinners = (year, offset, limit) => callApi(year + '/results/1', offset, limit);