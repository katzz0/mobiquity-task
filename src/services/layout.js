export const LAYOUT_SMALL = 'small';
export const LAYOUT_LARGE = 'large';

export const getLayout = () => {
    return window.innerWidth <= 1024 ? LAYOUT_SMALL : LAYOUT_LARGE;
};
