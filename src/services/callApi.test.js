import {makeQueryString} from './index';

it('should make query string', () => {
    expect(makeQueryString()).toBe('?limit=1000');
    expect(makeQueryString(10)).toBe('?offset=10&limit=1000');
    expect(makeQueryString(20, 40)).toBe('?offset=20&limit=40');
});
