import {createSelector} from 'reselect';
import {getSeason, getChampionsData} from './common';

export const getSeasonChampionDriverId = createSelector(
    [getSeason, getChampionsData],
    (season, data) => {
        const championEntry = data.find(entry => entry.season === season);
        return championEntry ? championEntry.driver.driverId: null;
    }
);
