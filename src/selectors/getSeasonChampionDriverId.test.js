import {getSeasonChampionDriverId} from './getSeasonChampionDriverId';
import driversList from '../test/driversList';

const championsData = driversList.map((driver, index) => ({driver, season: 2005 + index}));

it('should return champion\'s driver id for the selected season', () => {
    const driverId = getSeasonChampionDriverId({
        champions: {data: championsData},
        seasonWinners: {
            season: 2010,
        },
    });

    expect(driverId).toBe('vettel');
});