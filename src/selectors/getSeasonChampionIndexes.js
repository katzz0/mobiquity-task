import {createSelector} from 'reselect';
import {getSeasonWinnersData} from './common';
import {getSeasonChampionDriverId} from './getSeasonChampionDriverId';

export const getSeasonChampionIndexes = createSelector(
    [getSeasonChampionDriverId, getSeasonWinnersData],
    (championDriverId, data) => data.reduce((result, driver, index) => {
        if (driver.driverId === championDriverId) {
            result.push(index);
        }

        return result;
    }, []),
);
