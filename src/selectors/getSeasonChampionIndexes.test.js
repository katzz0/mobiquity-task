import {getSeasonChampionIndexes} from './getSeasonChampionIndexes';
import driversList from '../test/driversList';

const championDrivers = driversList.map((driver, index) => ({driver, season: 2005 + index}));

it('should return champion\'s indexes in the season winners list', () => {
    const indexes = getSeasonChampionIndexes({
        champions: {data: championDrivers},
        seasonWinners: {
            data: driversList,
            season: 2010,
        },
    });

    expect(indexes.length).toBe(1);
    expect(indexes).toEqual([5]);
});
