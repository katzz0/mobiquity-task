export const getSeason = (state, props) => state.seasonWinners.season;
export const getChampionsData = (state, props) => state.champions.data;
export const getSeasonWinnersData = (state, props) => state.seasonWinners.data;
