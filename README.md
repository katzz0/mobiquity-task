# The Mobiquity test task

You need `yarn` or `npm` installed for running this app

To fetch all dependencies run
````bash
yarn install
````

### Run app in dev environment with dev-server for development purposes

Run the following command
````bash
yarn start
````
and the default browser with an actual link will be open automatically.

### Build an application for production

Run the following command
```bash
yarn build
```
and the production ready app is in public folder.

### Run tests

Run the following command
```bash
yarn test
```
and the console window will be shown with test results.

# Notes

- The SPA is implemented without router for simplicity but it can be added easily. To solve the problem of showing
season winners `el.scrollIntoView` method is used. It don't supported by old browsers but works for
[majority](https://caniuse.com/#search=scrollIntoView). To improve UX animation to scrolling can be added or solution
with routing can also be used with transition animation.

- Offset / limit functionality is supported by the external service and its support has been added to data fetchers
in the project. Pagination functionality can be used for larger data sets but here just a simple solution with maximum
limit is used.

- Offset / limit for champions is enough complex solution since we need data for special years and the external service
can change data which it provides.

- Usage of TypeScript could facilitate usage of an external service since it provides enough complex data structures and
since it's external it's hard to control data consistency in cases of updates or security issues. The better solution is
to just covert external DTO into internal data models and TypeScript helps here very well or use som data validation libraries.
For simplicity in the current project simple check for data existence in the higher level is added but it doesn't check
internal structure.
